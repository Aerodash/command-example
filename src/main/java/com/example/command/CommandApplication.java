package com.example.command;

import com.example.command.core.CommandInvoker;
import com.example.command.core.Command;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;

@Slf4j
@SpringBootApplication
public class CommandApplication implements CommandLineRunner {

    private final CommandInvoker commandInvoker;

    public CommandApplication(CommandInvoker commandInvoker) {
        this.commandInvoker = commandInvoker;
    }

    // command.jar command sub-command params
    @Override
    public void run(String... args) {
        String commandName;
        String subCommandName;
        String[] commandArgs = new String[args.length];
        switch (args.length) {
            case 0:
                // Show help of all commands
                commandName = "help";
                subCommandName = "help";
                break;
            case 1:
                // Show help of command
                commandName = args[0];
                subCommandName = "help";
                break;
            case 2:
            default:
                // Execute subcommand
                commandName = args[0];
                subCommandName = args[1];
                commandArgs = Arrays.copyOfRange(args, 2, args.length);
                break;
        }

        log.info("Executing <Command={}, SubCommand={}, Args={}>", commandName, subCommandName, Arrays.toString(commandArgs));
        commandInvoker.invokeCommand(commandName, subCommandName, commandArgs);
    }

    public static void main(String[] args) {
        SpringApplication.run(CommandApplication.class, args);
    }
}
