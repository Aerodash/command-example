package com.example.command.commands;

import com.example.command.CustomParameter;
import com.example.command.annotation.Command;
import com.example.command.annotation.SubCommand;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Command(value = "test", description = "A command to test all use cases")
public class TestCommand {

    // command.jar test subCommand
    @SubCommand(description = "A sub command without parameters")
    public void subCommand() {
        log.info("TEST SUB COMMAND");
    }

    // command.jar test subCommand2 firstParamValue 2
    @SubCommand(description = "A sub command with parameters")
    public void subCommand2(String param1, int param2) {
        log.info("SUB COMMAND WITH PARAMS: {}, {}", param1, param2);
    }

    // command.jar test subCommand2 firstParamValue 2 2|4
    @SubCommand(description = "A sub command with a custom parameters")
    public void subCommand3(String param1, int param2, CustomParameter customParameter) {
        log.info("SUB COMMAND WITH PARAMS: {}, {}, {}", param1, param2, customParameter);
    }

}