package com.example.command;

import lombok.RequiredArgsConstructor;
import lombok.ToString;

/**
 * Represented as a String as following "attribute1|attribute2"
 */
@ToString
@RequiredArgsConstructor
public class CustomParameter {
    private final int attribute1;
    private final int attribute2;
}
