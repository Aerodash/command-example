package com.example.command.annotation;

import org.springframework.stereotype.Component;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface Command {
    // Name of the command
    String value();
    // Description of the command for the help message
    String description() default "";
}