package com.example.command.core;

import com.example.command.annotation.SubCommand;
import lombok.Getter;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import static org.springframework.core.annotation.AnnotationUtils.getAnnotation;
import static org.springframework.util.ReflectionUtils.doWithMethods;

@Component
class CommandRegistry implements ApplicationContextAware {

    @Getter
    private final Map<String, Command> commands = new TreeMap<>();

    @Override
    public void setApplicationContext(ApplicationContext ctx) throws BeansException {
        Class<com.example.command.annotation.Command> annotationClass = com.example.command.annotation.Command.class;
        Collection<Object> commandBeans = ctx.getBeansWithAnnotation(annotationClass).values();
        for (Object commandBean : commandBeans) {
            Class<?> commandClass = commandBean.getClass();
            String commandName = getAnnotation(commandClass, annotationClass).value();
            String commandDescription = getAnnotation(commandClass, annotationClass).description();
            Map<String, Command> subCommands = new HashMap<>();
            doWithMethods(commandClass, method -> {
                String subCommandName = getAnnotation(method, SubCommand.class).value();
                String subCommandDescription = getAnnotation(method, SubCommand.class).description();
                if (StringUtils.isEmpty(subCommandName)) {
                    subCommandName = method.getName();
                }
                Command subCommand = new Command(subCommandName, subCommandDescription, new CommandMethod(method, commandBean), null);
                subCommands.put(subCommandName, subCommand);
            }, method -> getAnnotation(method, SubCommand.class) != null);
            Command command = new Command(commandName, commandDescription, null, subCommands);
            commands.put(commandName, command);
        }
    }

    public Command getCommand(String commandName, String subCommandName) {
        return commands.get(commandName).getSubCommands().get(subCommandName);
    }
}
