package com.example.command.core;

public class ArgParserNotFoundException extends RuntimeException {
    public ArgParserNotFoundException(Class<?> notFoundType) {
        super(String.format("No argument parser was found for type: %s", notFoundType.getName()));
    }
}
