package com.example.command.core;

import org.springframework.stereotype.Service;

@Service
public class CommandInvoker {

    private final CommandRegistry commandRegistry;
    private final CompositeArgParser compositeArgParser;

    public CommandInvoker(CommandRegistry commandRegistry, CompositeArgParser compositeArgParser) {
        this.commandRegistry = commandRegistry;
        this.compositeArgParser = compositeArgParser;
    }

    public void invokeCommand(String commandName, String subCommandName, String... args) throws IllegalArgumentException {
        Command command = commandRegistry.getCommand(commandName, subCommandName);
        CommandMethod method = command.getCommandMethod();
        Class<?>[] parameterTypes = method.getParameterTypes();
        Object[] castArgs = new Object[0];
        if (args.length != parameterTypes.length) {
            throw new IllegalArgumentException(String.format("Wrong number of arguments. Expected: %d, Found: %d", parameterTypes.length, args.length));
        }
        if (args.length > 0) {
            castArgs = parseArgs(args, parameterTypes);
        }

        method.invoke(castArgs);
    }

    private Object[] parseArgs(String[] stringArgs, Class<?>[] parameterTypes) {
        Object[] args = new Object[parameterTypes.length];
        for (int i = 0; i < parameterTypes.length; i++) {
            args[i] = compositeArgParser.parse(stringArgs[i], parameterTypes[i]);
        }
        return args;
    }

}
