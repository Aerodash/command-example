package com.example.command.core;

import com.example.command.annotation.Command;
import com.example.command.annotation.SubCommand;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import java.util.Map;

@Slf4j
@Command(value = "help", description = "Displays help for all commands.")
public class HelpCommand {

    private final CommandRegistry commandRegistry;

    public HelpCommand(CommandRegistry commandRegistry) {
        this.commandRegistry = commandRegistry;
    }

    @SubCommand
    public void help() {
        Map<String, com.example.command.core.Command> commands = commandRegistry.getCommands();
        log.info("List of available commands:");
        commands.forEach((name, command) -> {
            String description = command.getDescription();
            if (StringUtils.isEmpty(description)) {
                log.info("\t- {}", name);
            } else {
                log.info("\t- {}: {}", name, description);
            }

            if (!"help".equals(name)) {
                command.getSubCommands().forEach((subCommandName, subCommand) -> {
                    String subCommandDescription = subCommand.getDescription();
                    if (StringUtils.isEmpty(subCommandDescription)) {
                        log.info("\t\t* {}", subCommandName);
                    } else {
                        log.info("\t\t* {}: {}", subCommandName, subCommandDescription);
                    }
                });
            }
        });
    }
}
