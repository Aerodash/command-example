package com.example.command.core;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.ParameterizedType;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 1- Handles all primitive types + String.
 * 2- Injects all ArgParser instances and acts as a single centralized parser
 */
@Service
@SuppressWarnings("rawtypes")
class CompositeArgParser {

    private static final Map<Class<?>, Class<?>> PRIMITIVES_TO_WRAPPERS;
    private final Map<Class<?>, ArgParser<?>> parsersByType = new HashMap<>();
    private final Map<Class<?>, ArgParser<?>> defaultParsers = new HashMap<>();

    public CompositeArgParser(@Autowired(required = false) List<ArgParser> parsers) {
        initializeDefaultParsers();
        initializeUserDefinedParsers(parsers);
    }

    private void initializeDefaultParsers() {
        defaultParsers.put(String.class, stringValue -> stringValue);
        defaultParsers.put(Integer.class, Integer::parseInt);
        defaultParsers.put(Double.class, Double::parseDouble);
        defaultParsers.put(Float.class, Float::parseFloat);
        defaultParsers.put(Character.class, stringValue -> stringValue.charAt(0));
        defaultParsers.put(Short.class, Short::parseShort);
        defaultParsers.put(Long.class, Long::parseLong);
        defaultParsers.put(Boolean.class, Boolean::parseBoolean);
    }

    private void initializeUserDefinedParsers(List<ArgParser> parsers) {
        if (parsers == null || parsers.size() == 0) {
            return;
        }

        for (ArgParser parser : parsers) {
            Class<? extends ArgParser> clazz = parser.getClass();
            Class<?> argType = getInterfaceGenericType(clazz);
            parsersByType.put(argType, parser);
        }
    }

    private Class<?> getInterfaceGenericType(Class<?> clazz) {
        return (Class<?>) ((ParameterizedType) clazz.getGenericInterfaces()[0]).getActualTypeArguments()[0];
    }

    public Object parse(String stringValue, Class<?> targetType) {
        ArgParser parser = defaultParsers.get(targetType);
        if (targetType.isPrimitive()) {
            parser = defaultParsers.get(wrap(targetType));
        }

        if (parser == null) {
            parser = parsersByType.get(targetType);
        }

        if (parser == null) {
            throw new ArgParserNotFoundException(targetType);
        }

        return parser.parse(stringValue);
    }

    @SuppressWarnings("unchecked")
    private static <T> Class<T> wrap(Class<T> c) {
        return c.isPrimitive() ? (Class<T>) PRIMITIVES_TO_WRAPPERS.get(c) : c;
    }

    static {
        Map<Class<?>, Class<?>> primToWrap = new HashMap<>();
        primToWrap.put(boolean.class, Boolean.class);
        primToWrap.put(byte.class, Byte.class);
        primToWrap.put(char.class, Character.class);
        primToWrap.put(double.class, Double.class);
        primToWrap.put(float.class, Float.class);
        primToWrap.put(int.class, Integer.class);
        primToWrap.put(long.class, Long.class);
        primToWrap.put(short.class, Short.class);
        primToWrap.put(void.class, Void.class);
        PRIMITIVES_TO_WRAPPERS = Collections.unmodifiableMap(primToWrap);
    }
}
