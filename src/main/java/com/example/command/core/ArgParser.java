package com.example.command.core;

/**
 * Can implement your own parser for types other than primitives and String
 */
@FunctionalInterface
public interface ArgParser<T> {
    T parse(String stringValue);
}
