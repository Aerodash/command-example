package com.example.command.core;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

@Slf4j
@Getter
@RequiredArgsConstructor
public class Command {
    private final String name;
    private final String description;
    private final CommandMethod commandMethod;
    private final Map<String, Command> subCommands;
}
