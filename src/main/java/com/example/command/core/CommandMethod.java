package com.example.command.core;

import lombok.RequiredArgsConstructor;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

@RequiredArgsConstructor
class CommandMethod {
    private final Method method;
    private final Object target;

    public void invoke(Object... args) {
        try {
            method.invoke(target, args);
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public Class<?>[] getParameterTypes() {
        return method.getParameterTypes();
    }
}
