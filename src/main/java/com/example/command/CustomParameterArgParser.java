package com.example.command;

import com.example.command.core.ArgParser;
import org.springframework.stereotype.Component;

@Component
public class CustomParameterArgParser implements ArgParser<CustomParameter> {
    @Override
    public CustomParameter parse(String stringValue) {
        String[] splitValue = stringValue.split("\\|");
        int attribute1 = Integer.parseInt(splitValue[0]);
        int attribute2 = Integer.parseInt(splitValue[1]);
        return new CustomParameter(attribute1, attribute2);
    }
}
